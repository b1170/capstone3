//react-router-dom 5.3.0
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

// React Context
import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';


// components
import Navigation from './components/Navigation';
import Footer from './components/Footer';

// pages
import Home from './pages/Home';
import LoginUser from './pages/SignIn';
import Shop from './pages/Shop';
import Register from './pages/Register';
import ShopItem from './pages/ShopItem';
import EditItem from './pages/EditItem';
import Logout from './pages/Logout';


// css
import "bootswatch/dist/yeti/bootstrap.min.css";




function App() {

  let token = localStorage.getItem("token")

  //set user details
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  //remove user details
  const unsetUser = () => {
    localStorage.clear();

    setUser({
      id: null,
      isAdmin: null
    })
  }

  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/api/user/user-details`, {
      headers: {
        'Access-Control-Allow-Origin': 'http://localhost:3000',
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Allow-Methods': 'POST, GET',
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`
      }
    })
      .then(response => response.json())
      .then(data => {

        if (typeof data._id !== "undefined") {
          setUser({
            id: data._id,
            isAdmin: data.isAdmin

          })

        }
      })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  //set accessible routes
  let routes;

  if (user.isAdmin === true) {
    //admin routes
    routes = (
      <>
        <Route exact path="/"><Home /></Route>
        <Route exact path="/shop"><Shop /></Route>
        <Route exact path="/shop/:productId"><ShopItem /></Route>
        <Route exact path="/admin/edit/:productId"><EditItem /></Route>
        <Route exact path="/logout" ><Logout /></Route>
      </>
    )
  } else if (user.isAdmin === false) {
    routes = (
      //logged in but not admin
      <>
        <Route exact path="/"><Home /></Route>
        <Route exact path="/shop"><Shop /></Route>
        <Route exact path="/shop/:productId"><ShopItem /></Route>
        <Route exact path="/logout" ><Logout /></Route>
      </>
    )
  } else {
    routes = (
      //not logged in
      <>
        <Route exact path="/"><Home /></Route>
        <Route exact path="/shop"><Shop /></Route>
        <Route exact path="/signin"><LoginUser /></Route>
        <Route exact path="/register"><Register /></Route>

      </>
    )
  }

  return (

    <UserProvider value={{ user, setUser, unsetUser }}>

      <Router>
        <Navigation />
        <Switch>

          {routes}

        </Switch>
        <Footer />
      </Router>

    </UserProvider>
  );
}

export default App;
