import React from 'react'

const UserContext = React.createContext("");

export const UserProvider = UserContext.Provider;

// eslint-disable-next-line import/no-anonymous-default-export
export default UserContext;