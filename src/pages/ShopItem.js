import { useEffect, useState, useContext } from 'react'
import { Container, Row, Col, Button, Image } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom'

import UserContext from './../UserContext'

import Swal from "sweetalert2";

export default function ShopItem() {


    const [name, setName] = useState("")
    const [description, setDesc] = useState("")
    const [caption, setCaption] = useState("")
    const [image, setImage] = useState("")
    const [price, setPrice] = useState(0)
    const [quantity, setQty] = useState(0)
    const [weight, setWeight] = useState(0)
    const [isActive, setIsActive] = useState(false)

    const { user } = useContext(UserContext)
    const { productId } = useParams()

    // const [cart, setCart] = useState({})

    const fetchProduct = () => {
        fetch(`${process.env.REACT_APP_API_URL}/api/product/find/${productId}`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        }
        ).then(response => response.json()).then(items => {

            setName(items.name)
            setImage(items.image)
            setDesc(items.description)
            setCaption(items.caption)
            setPrice(items.price)
            setWeight(items.weight)
            setQty(items.quantity)
            setIsActive(items.isActive)
        })
    }

    useEffect(() => {
        fetchProduct()

    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])


    // Set how buttons are shown
    let buttons;

    // add to cart
    // const addToCart = (productId) => {
    //     setCart(localStorage.setItem(productId))
    // }

    // console.log(cart)

    if (user.id !== null && user.isAdmin !== true) {
        buttons = (
            <>
                <Button className='btn btn-small btn-primary p-2 my-1'  >Add to cart</Button>

            </>
        )
    } else if (user.isAdmin === true) {

        if (isActive === true) {
            buttons = (
                <>
                    <Link className='btn btn-small btn-primary p-2 my-1' to={`/admin/edit/${productId}`} >Edit</Link>
                    <Button className='btn btn-small btn-warning p-2 my-1' onClick={() => disableProduct(productId, isActive)}>Disable</Button>
                </>
            )
        } else if (isActive !== true) {

            buttons = (
                <>
                    <Link className='btn btn-small btn-primary p-2 my-1' to={`/admin/edit/${productId}`} >Edit</Link>
                </>
            )
        }
    }

    // disable product
    const disableProduct = (productId, isActive) => {
        fetch(`${process.env.REACT_APP_API_URL}/api/product/archive/${productId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                isActive: !isActive
            })
        }).then(response => response.json()).then(result => {
            if (typeof result !== undefined) {
                fetchProduct()

                Swal.fire({
                    text: "Product has been disabled."
                })
            } else {
                Swal.fire({
                    text: "Something went wrong."
                })
                fetchProduct()
            }
        })
    }

    return (

        <>

            {/* item view */}
            <Container className='my-3'>
                <Row>
                    <Col lg={4}>
                        <Image fluid src={image} rounded />
                    </Col>
                    <Col lg={8} className='p-3 text-center'>
                        <Row className='p-4'>
                            <h2><strong>{name}</strong></h2>
                        </Row>
                        <Row className='p-4'>
                            <h5>{caption}</h5>
                        </Row>
                        <Row className='p-4'>
                            <h3>{description}</h3>
                        </Row>
                        <Row className='p-4'>
                            <Col>
                                <p>PhP {price}</p>
                            </Col>
                            <Col><p>Weight: {weight} grams</p></Col>
                            <Col><p>Stocks: {quantity} available</p></Col>
                        </Row>
                        <Row>
                            {buttons}
                        </Row>
                    </Col>
                </Row>


            </Container>

        </>
    )


}