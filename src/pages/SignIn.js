import React, { useState, useEffect, useContext } from 'react'
import UserContext from '../UserContext';
import { Container, Row, Col } from "react-bootstrap";
import { Redirect, useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'
import Home from './Home';

export default function LoginUser() {

    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isDisabled, setIsDisabled] = useState(true)
    const history = useHistory()

    useEffect(() => {

        if (email !== "" && password !== "") {
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        }

    }, [email, password])

    function Login(e) {
        e.preventDefault()

        fetch(` ${process.env.REACT_APP_API_URL}/api/user/login`, {
            crossDomain: true,
            method: "POST",
            headers: {
                'Access-Control-Allow-Origin': 'http://localhost:3000',
                'Access-Control-Allow-Credentials': true,
                'Access-Control-Allow-Methods': 'POST, GET',
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(response => response.json())
            .then(data => {

                if (data === false) {

                    //alert the user that login failed
                    Swal.fire({
                        icon: 'warning',
                        text: 'No account found. Register first',
                        timer: 2000
                    })

                } else {

                    //store data in local storage
                    localStorage.setItem("token", data.access)
                    //invoke the function to retrive user details
                    userDetails(data.access)

                    //alert the user that login is successful
                    Swal.fire({
                        icon: 'success',
                        text: 'Logged in!'
                    })
                    history.push("/")
                }
            })

        setEmail("")
        setPassword("")
    }

    const userDetails = (token) => {

        //send request to the server
        fetch(` ${process.env.REACT_APP_API_URL}/api/user/user-details`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
            .then(response => response.json())
            .then(data => {
                //use setUser() to update the state
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
    }


    return (

        (!user) ? <Redirect to="/" component={Home} /> :
            <Container fluid>
                <Row className="justify-content-center">
                    <Col lg={5} className="border-right p-5">
                        <fieldset />
                        <legend>Login</legend>
                        <form onSubmit={(e) => Login(e)}>
                            {/* login email */}
                            <div className="form-group">
                                <label htmlFor="inputLoginEmail1" className="form-label mt-4">Email address</label>
                                <input type="email" className="form-control" id="inputLoginEmail1"
                                    value={email}
                                    aria-describedby="emailHelp" placeholder="Enter email"
                                    onChange={(e) => setEmail(e.target.value)}
                                />
                            </div>
                            {/* login password */}
                            <div className="form-group">
                                <label htmlFor="loginPassword1" className="form-label mt-4">Password</label>
                                <input type="password" className="form-control"
                                    value={password} id="loginPassword1" placeholder="Password"
                                    onChange={(e) => setPassword(e.target.value)}
                                />
                            </div>
                            <div className="mt-3">
                                <button type="submit" className="btn btn-primary" disabled={isDisabled}>Submit</button>
                            </div>

                            <div className="mt-2">
                                <small><strong>No account yet? <a href="/register">Register here.</a></strong></small>
                            </div>
                        </form>
                    </Col>
                </Row>
            </Container>
    )
}