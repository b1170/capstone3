import { Row, Col, Container } from 'react-bootstrap'
import { useState, useEffect } from 'react'
import Swal from 'sweetalert2'

export default function Register() {

    const [username, setUserName] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [cpw, setCpw] = useState("")

    const [isDisabled, setIsDisabled] = useState(true)

    useEffect(() => {
        if ((email !== "" && password !== "" && cpw !== "") &&
            (password === cpw)) {
            setIsDisabled(false)
        } else {
            setIsDisabled(true)
        }

    }, [email, password, cpw])


    function Register(e) {
        e.preventDefault()

        //check if the values are successfully bounded.
        console.log(email);
        console.log(password);
        console.log(cpw);

        //send a request to our API collection
        //apply an environment variable in our fetch request.
        fetch(`${process.env.REACT_APP_API_URL}/api/user/register`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                email: email,
                password: password
            })
        }).then(response => response.json()).then(convertedData => {
            console.log(convertedData);

            //create a control structure
            if (convertedData === true) {
                setUserName("")
                setEmail("")
                setPassword("")
                setCpw("")
                //successful 
                Swal.fire({
                    icon: 'success',
                    text: 'Created Account. Login Now.'
                })

            } else {
                //unsuccessful 
                Swal.fire({
                    icon: 'error',
                    text: 'Something Went Wrong. Try again?'
                })
            }
        })
    }

    return (

        <Container fluid>
            <Row className="justify-content-center">
                <Col lg={5} className="border-right p-5">
                    <fieldset />
                    <legend>Register</legend>
                    <form onSubmit={(e) => Register(e)}>
                    <div className="form-group">
                            <label htmlFor="inputRegisterUser1" className="form-label mt-4">User name</label>
                            <input type="text" className="form-control" id="inputRegisterUser1" aria-describedby="userHelp" placeholder="Enter username"
                                value={username}
                                onChange={(e) => setUserName(e.target.value)} />
                        </div>
                        {/* email */}
                        <div className="form-group">
                            <label htmlFor="inputRegisterEmail1" className="form-label mt-4">Email address</label>
                            <input type="email" className="form-control" id="inputRegisterEmail1" aria-describedby="emailHelp" placeholder="Enter email"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)} />
                        </div>
                        {/* password */}
                        <div className="form-group">
                            <label htmlFor="registerPassword1" className="form-label mt-4">Password</label>
                            <input type="password" className="form-control" id="registerPassword1" placeholder="Password"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)} />
                        </div>
                        {/* confirm password */}
                        <div className="form-group">
                            <label htmlFor="confirmRegisterPassword1" className="form-label mt-4">Confirm Password</label>
                            <input type="password" className="form-control" id="confirmRegisterPassword1" placeholder="Password"
                                value={cpw}
                                onChange={(e) => setCpw(e.target.value)} />
                        </div>
                        <div className="mt-3">
                            <button type="submit" className="btn btn-primary" disabled={isDisabled}>Submit</button>
                        </div>

                        <div className="mt-2">
                            <small><strong>Have an account? <a href="/signin">Login here.</a></strong></small>
                        </div>
                    </form>
                </Col>
            </Row>
        </Container>
    )
}
