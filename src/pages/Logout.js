import { useContext, useEffect } from 'react';
import {Redirect} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Logout() {
	const { unsetUser, setUser } = useContext(UserContext);
	
	//Clear the localStorage
	unsetUser();
	
	useEffect(() => {
		//Set the user state back to it's original value
		setUser({id: null, isAdmin: null})
		Swal.fire({
			icon:'success',
			text:`You're now logged out.`,
			timer: 1500
		})
		
	})

	return(
		<>
		<Redirect exact to="/" />
		</>
	)
}
