import { Container, Row, Col, Image, Form, Button } from 'react-bootstrap'
import {Link, useHistory} from 'react-router-dom'
import { useEffect, useState } from 'react'
import Swal from 'sweetalert2'

export default function EditItem() {

    const url = (window.location.pathname)
    const [productId] = useState(url.split("/").pop())
    const [items, setItems] = useState({})

    const [name, setName] = useState("")
    const [image, setImage] = useState("")
    const [description, setDesc] = useState("")
    const [caption, setCaption] = useState("")
    const [price, setPrice] = useState()
    const [weight, setWeight] = useState()
    const [quantity, setQty] = useState()

    const history = useHistory();
    useEffect(() => {

        fetch(`${process.env.REACT_APP_API_URL}/api/product/find/${productId}`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
        }
        ).then(response => response.json()).then(items => {
            setItems(items)
        })
    }, [productId])

    

    // edit product
    const editProduct = (e, productId) => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/api/product/update/${productId}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                name: name,
                image: image,
                description: description,
                caption: caption,
                price: price,
                weight: weight,
                quantity: quantity
            })
        }).then(response => response.json()).then(data => {
            if(data !== "undefined") {
                console.log(data)
                Swal.fire({
                    icon:"success",
                    text: `${name} has been updated.`
                }) 
                history.push(`/shop/${productId}`)
            } else {
                Swal.fire({
                    icon:"warning",
                    text: "Something went wrong, try again?"
                })
            }
        })
    }

    return (

        <>
            <Container className='my-3'>
                <Row>
                    <Col lg={4}>
                        <Image fluid src={items.image} alt={items.name}rounded />
                    </Col>
                    <Col lg={8}>

                        <Container fluid className='m-3'>
                            <Row className='justify-content-center'> <h3>Edit Product: {items.name}</h3>
                            </Row>

                        </Container>
                        <Form onSubmit={(e) => editProduct(e, productId)}>
                            <Form.Group as={Row} controlId="productName" className='mt-1'>
                                <Form.Label column sm={2}>
                                    Product Name
                                </Form.Label>
                                <Col sm={10}>
                                    <Form.Control type="text" placeholder={items.name}
                                    onChange={(e) => setName(e.target.value)} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="productImage" className='mt-1'>
                                <Form.Label column sm={2}>
                                    Image
                                </Form.Label>
                                <Col sm={10}>
                                    <Form.Control type="text" placeholder={items.image}
                                    onChange={(e) => setImage(e.target.value)} />
                                </Col>
                            </Form.Group>
                            
                            <Form.Group as={Row} controlId="productDesc" className='mt-1'>
                                <Form.Label column sm={2}>
                                    Product Description
                                </Form.Label>
                                <Col sm={10}>
                                    <Form.Control as="textarea" rows={3} placeholder={items.description}
                                    onChange={(e) => setDesc(e.target.value)} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="productCaption" className='mt-1'>
                                <Form.Label column sm={2}>
                                    Caption
                                </Form.Label>
                                <Col sm={10}>
                                    <Form.Control type="text" placeholder={items.caption}
                                    onChange={(e) => setCaption(e.target.value)} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="productPrice" className='mt-1'>
                                <Form.Label column sm={2}>
                                    Price
                                </Form.Label>
                                <Col sm={10}>
                                    <Form.Control type="number" placeholder={items.price}
                                    onChange={(e) => setPrice(e.target.value)} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="productWeight" className='mt-1'>
                                <Form.Label column sm={2}>
                                    Weight (grams)
                                </Form.Label>
                                <Col sm={10}>
                                    <Form.Control type="number" placeholder={items.weight}
                                    onChange={(e) => setWeight(e.target.value)} />
                                </Col>
                            </Form.Group>
                            <Form.Group as={Row} controlId="productQuantity" className='mt-1'>
                                <Form.Label column sm={2}>
                                    Quantity
                                </Form.Label>
                                <Col sm={10}>
                                    <Form.Control type="number" placeholder={items.quantity}
                                    onChange={(e) => setQty(e.target.value)} />
                                </Col>
                            </Form.Group>
                            <Container fluid className='m-3'>

                            <Button className='m-1 p-2 btn btn-success' type='submit'>Save</Button>

                            <Link className='m-1 p-2 btn btn-warning' to={`/shop`}>Cancel</Link>

                        </Container>
                        </Form>
                        
                    </Col>
                </Row>
            </Container>
        </>
    )
}