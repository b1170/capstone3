
import ShopView from "../components/ShopView"
import { useState, useEffect } from 'react'


export default function Shop() {
    const [product, setProduct] = useState([])
    
    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/api/product/list-products`).then(response => response.json()).then(data => {
            setProduct(data)
        })
    }

    useEffect(() => {
        fetchData()
    }, [])


    return (
        <>
            <ShopView productData={product} fetchData={fetchData} />
        </>
    )

}