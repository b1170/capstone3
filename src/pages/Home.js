import { Container } from 'react-bootstrap';


// components
import Banner from './../components/Banner';
import Featured from './../components/Featured'

export default function Home() {


    return (
        <>
            <Container>
                <Banner />
                <Featured />
            </Container>
        </>

    )

}
