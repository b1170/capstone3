import { Container, Row, Col } from 'react-bootstrap'
import { useState } from 'react'
import ShopCard from '../components/ShopCard'

export default function Admin() {
    
    
    const [prodName, setProdName] = useState("")
    const [desc, setProdDesc] = useState("")
    const [price, setPrice] = useState(0)
    const [quantity, setQty] = useState(0)
    const [capt, setCapt] = useState("")
    const [weight, setWeight] = useState("")
    const [category, setCategory] = useState("")
    const [isFeatured, setIsFeatured] = useState(false)




    return (
        <Container fluid>

 
            <ShopCard />

            {/* modal */}
            <div className="modal">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title">Modal title</h5>
                            <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true"></span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <Row className="justify-content-center">
                                <Col lg={5} className="border-right p-5">
                                    <form >
                                        <div className="form-group">
                                            <label htmlFor="prodName" className="form-label mt-4"><small><strong>Product Name</strong></small></label>
                                            <input type="text" className="form-control" id="prodName" placeholder="Product Name"
                                                value={prodName}
                                                onChange={(e) => setProdName(e.target.value)} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="desc" className="form-label mt-4"><small><strong>Product Description</strong></small></label>
                                            <textarea className="form-control" id="desc" rows="3"
                                                value={desc}
                                                onChange={(e) => setProdDesc(e.target.value)}
                                            ></textarea>
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="capt" className="form-label mt-4"><small><strong>Product Caption</strong></small></label>
                                            <input type="text" className="form-control" id="capt" placeholder="Product Name"
                                                value={capt}
                                                onChange={(e) => setCapt(e.target.value)} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="price" className="form-label mt-4"><small><strong>Product Price</strong></small></label>
                                            <input type="number" className="form-control" id="price" placeholder="Price"
                                                value={price}
                                                onChange={(e) => setPrice(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="qty" className="form-label mt-4"><small><strong>Product Quantity</strong></small></label>
                                            <input type="number" className="form-control" id="qty" placeholder="Qty"
                                                value={quantity}
                                                onChange={(e) => setQty(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="weight" className="form-label mt-4"><small><strong>Product Weight</strong></small></label>
                                            <input type="number" className="form-control" id="weight" placeholder="Weight"
                                                value={weight}
                                                onChange={(e) => setWeight(e)} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="category" className="form-label mt-4"><small><strong>Product Category</strong></small></label>
                                            <input type="text" className="form-control" id="catgory" placeholder="Category"
                                                value={category}
                                                onChange={(e) => setCategory} />
                                        </div>
                                        <div className="form-check mt-3">
                                            <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                            <label className="form-check-label" htmlFor="flexCheckDefault"
                                                value={isFeatured}
                                                onChange={(e) => setIsFeatured(e)}
                                            >
                                                <strong>Featured Product?</strong>
                                            </label>
                                        </div>
                                        <div className='mt-3'>
                                            <button className='btn btn-primary' type='submit'>Upload</button>
                                            <button className='mx-3 btn btn-danger' type='clear'>Clear</button>
                                        </div>
                                    </form>
                                </Col>
                            </Row>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary">Save changes</button>
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>

        </Container>

    )

}