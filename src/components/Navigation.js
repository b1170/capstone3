import { useContext } from 'react';
import { Navbar, Container, Nav } from 'react-bootstrap'
import UserContext from '../UserContext';


export default function Navigation() {
    const { user } = useContext(UserContext)
    let links;

    if (user.id !== null && user.isAdmin !== true) {
        links = (
            //non-admin includes checkout
            <>
                <Navbar expand="lg" variant="light">
                    <Navbar.Brand href="/" className='p-2'>Pet Shop</Navbar.Brand>
                </Navbar>


                <Navbar.Toggle className='mr-auto' aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Container fluid className='d-flex justify-content-lg-end'>
                        <Nav className="ml-auto">
                            <Nav.Link href="/">Home</Nav.Link>
                            <Nav.Link href="/shop">Shop</Nav.Link>
                            <Nav.Link href="/logout">Logout</Nav.Link>
                        </Nav>
                    </Container>
                </Navbar.Collapse>
            </>
        )
    } else if (user.isAdmin === true) {
        //admin does not include checkout
        links = (
        <>
            <Navbar expand="lg" variant="light">
                <Navbar.Brand href="/" className='p-2'>Pet Shop</Navbar.Brand>
            </Navbar>


            <Navbar.Toggle className='mr-auto' aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Container fluid className='d-flex justify-content-lg-end'>
                <Nav className="ml-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                        <Nav.Link href="/shop">Admin</Nav.Link>
                        <Nav.Link href="/logout">Logout</Nav.Link>
                    </Nav>
                </Container>
            </Navbar.Collapse>
        </>
        )

    } else {
        links = (
            
            // logged out user doesn't see logout and checkout
            <>
                <Navbar expand="lg" variant="light">
                    <Navbar.Brand href="/" className='p-2'>Pet Shop</Navbar.Brand>
                </Navbar>


                <Navbar.Toggle className='mr-auto' aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Container fluid className='d-flex justify-content-lg-end'>
                        <Nav className="ml-auto">
                            <Nav.Link href="/">Home</Nav.Link>
                            <Nav.Link href="/shop">Shop</Nav.Link>
                            <Nav.Link href="/register">Register</Nav.Link>
                            <Nav.Link href="/signin">Login</Nav.Link>
                        </Nav>
                    </Container>
                </Navbar.Collapse>
            </>
        )
    }

    return (

        <Navbar className='navbar-dark bg-primary' expand="lg">

            {links}

        </Navbar>

    )
}

