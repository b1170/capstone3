import { Carousel, Container, Image } from "react-bootstrap"



export default function Banner() {
    return (

        <Container>
            <Carousel className="mt-3">
                <Carousel.Item>
                    <Image fluid src={require('./../images/erik-jan-leusink-IbPxGLgJiMI-unsplash.jpg')} />
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                    </Carousel.Caption>
                </Carousel.Item>

                <Carousel.Item>
                    <Image fluid src={require('./../images/erik-jan-leusink-IbPxGLgJiMI-unsplash.jpg')} />
                    <Carousel.Caption>
                        <h3>Second slide label</h3>
                    </Carousel.Caption>
                </Carousel.Item>

                <Carousel.Item>
                    <Image fluid src={require('./../images/erik-jan-leusink-IbPxGLgJiMI-unsplash.jpg')} />
                    <Carousel.Caption>
                        <h3>Third slide label</h3>
                    </Carousel.Caption>
                </Carousel.Item>

            </Carousel>
        </Container>

    )
}

