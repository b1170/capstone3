import {
    Image,
    Container,
    Row,
    Col
} from 'react-bootstrap'

export default function Category () {

    return (

        <Container fluid className='mt-3'>
            <Row>
                <Col xs={6} lg={3} className='p-3'>
                    <Row>
                    <Image fluid roundedCircle src={require('./../images/karsten-winegeart-oU6KZTXhuvk-unsplash.jpg')} />
                    </Row>
                    <Row className='p-3 text-center'>
                        <small><strong>Category 1</strong></small>
                    </Row>
                </Col>
 
                <Col xs={6} lg={3} className='p-3'>
                    <Image fluid roundedCircle src={require('./../images/karsten-winegeart-oU6KZTXhuvk-unsplash.jpg')} />
                    <Row className='p-3 text-center'>
                        <small><strong>Category 2</strong></small>
                    </Row>
                </Col>

                <Col xs={6} lg={3} className='p-3'>
                    <Image fluid roundedCircle src={require('./../images/karsten-winegeart-oU6KZTXhuvk-unsplash.jpg')} />
                    <Row className='p-3 text-center'>
                        <small><strong>Category 3</strong></small>
                    </Row>
                </Col>
                
                <Col xs={6} lg={3} className='p-3'>
                    <Image fluid roundedCircle src={require('./../images/karsten-winegeart-oU6KZTXhuvk-unsplash.jpg')} />
                    <Row className='p-3 text-center'>
                        <small><strong>Category 4</strong></small>
                    </Row>
                </Col>
            </Row>
        </Container>
        
    )
}
