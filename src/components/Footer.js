import { Navbar, Nav, Container } from 'react-bootstrap'

export default function Footer() {


    return (

        <Navbar className='navbar-dark bg-primary sticky-bottom'>

            <Container fluid className='d-flex justify-content-center'>
                <Nav className="ml-auto">
                    <Nav.Link href="/">About</Nav.Link>
                    <Nav.Link href="/">Contact</Nav.Link>
                    
                </Nav>
            </Container>

        </Navbar>


    )



}
