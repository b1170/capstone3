import { useContext } from 'react'
import { Col, Row, Figure, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function ShopCard({productProp}) {
    
    const { name, caption, image, price, isActive, _id } = productProp
    const { user } = useContext(UserContext)



    let buttons;

    const activeProduct = () => {
        if (user.isAdmin === true) {
            if (isActive === true) {
                return (
                    <>
                        <p className="text-success"><small><strong>Active Product</strong></small></p>
                    </>
                )
            } else {
                return (
                    <>
                        <p className="text-danger"><small><strong>Inactive Product</strong></small></p>
                    </>
                )
            }
        }
    }

    const logoutOnClickReminder = () => {
        Swal.fire({
            text: 'Please log in'
        })
    }

    if (user.id !== null) {
        if (user.isAdmin !== true) {
            buttons = (
                <>
                    <Link className='btn btn-small btn-primary p-1 my-1' to={`/shop/${_id}`}><small>View Item</small></Link>

                </>
            )
        } else if (user.isAdmin === true) {

            buttons = (
                <>
                    <Link className='btn btn-small btn-primary p-1 my-1' to={`/shop/${_id}`}><small>View Item</small></Link>
                </>
            )
        }
    } else {
        buttons = (
            //set a message that warns they need to log in first
            <Button className='btn btn-small btn-primary p-1 my-1' onClick={logoutOnClickReminder}>View Item</Button>
        )
    }

    return (

        <>
            
            {/* item cards */}
            <Col lg={2} className='p-3'>
                <Row>
                    <Figure.Image
                        width={150}
                        height={150}
                        alt="Test"
                        src={image} /></Row>
                <Row className="p-3">
                    <Figure.Caption>
                        <small><strong>{name}</strong></small>
                    </Figure.Caption>
                    <Figure.Caption>
                        <p><small>{caption}</small></p>
                    </Figure.Caption>
                    <Figure.Caption>
                        <p><small><strong>PhP {price}</strong> </small></p>
                    </Figure.Caption>
                    <Figure.Caption>
                        {activeProduct(isActive)}
                    </Figure.Caption>

                    {buttons}

                </Row>
            </Col>
            </>

    )
}