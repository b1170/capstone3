import { Container, Modal, Button, Form, Col, Row } from "react-bootstrap";
import { useContext, useEffect, useState } from 'react'
import ShopCard from './ShopCard'
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function ShopView(props) {
    
    const {productData} = props
    const { user } = useContext(UserContext)
    const [product, setProduct] = useState([])
    const [modalShow, setModalShow] = useState(false)

    const modalClose = () => setModalShow(false);
    const modalOpen = () => setModalShow(true);

    const [name, setName] = useState("")
    const [image, setImage] = useState("")
    const [description, setDesc] = useState("")
    const [caption, setCaption] = useState("")
    const [price, setPrice] = useState()
    const [weight, setWeight] = useState()
    const [quantity, setQty] = useState()

    useEffect(() => {
        const prodArr = productData.map(product => {

            if (user.isAdmin === true) {
                //returns all products
                return (
                    <ShopCard productProp={product} key={product._id} />
                )
            } else if (!user.isAdmin && product.isActive !== false) {
                //shows active products only 
                return (
                    <ShopCard productProp={product} key={product._id} />
                )
            } else {
                return null
            }
        })

        setProduct(prodArr)
    }, [user, productData])

    let addButton;

    //only shows Add button if you're an admin
    if (user.isAdmin === true) {
        addButton = (
            <Container>

                <Container className="d-flex justify-content-end mt-3">
                    <Col className="bg-info"><h3 className="text-white text-center">ADMIN PANEL</h3></Col>
                    <Button variant="primary" onClick={modalOpen}>Add new product</Button>
                </Container>

            </Container>
        )
    }

    //add new product
    const addNewProduct = (e) => {
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/api/product/add-products`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                name: name,
                image: image,
                description: description,
                caption: caption,
                price: price,
                weight: weight,
                quantity: quantity
            })
        }).then(response => response.json()).then(data => {
            if (data !== null) {
                Swal.fire({
                    icon: "success",
                    text: `Successfully created new product: ${name}`
                }).then(function() {window.location.reload()}) 
            } else {
                Swal.fire({
                    icon:"error",
                    text: "Something went wrong. Contact your admin"
                })
            }
        })

    }

    return (

        <>
            {addButton}

            {/* product list */}
            <Container className="d-flex flex-wrap">
                {product}
            </Container>

            {/* modal container */}
            <Modal show={modalShow} onHide={modalClose} centered>
                <Modal.Header closeButton>
                    <Modal.Title>Add a new product</Modal.Title>
                </Modal.Header>
                <Modal.Body>

                    <Form onSubmit={(e) => addNewProduct(e)}>
                        <Form.Group as={Row} controlId="productName" className='mt-1'>
                            <Form.Label column sm={2}>
                                Product Name
                            </Form.Label>
                            <Col sm={10}>
                                <Form.Control type="text" placeholder="Enter Product Name"
                                    onChange={(e) => setName(e.target.value)}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="productImage" className='mt-1'>
                            <Form.Label column sm={2}>
                                Image
                            </Form.Label>
                            <Col sm={10}>
                                <Form.Control type="text" placeholder="Enter Image Link"
                                    onChange={(e) => setImage(e.target.value)}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="productDesc" className='mt-1'>
                            <Form.Label column sm={2}>
                                Product Description
                            </Form.Label>
                            <Col sm={10}>
                                <Form.Control as="textarea" rows={3} placeholder="Enter Description"
                                    onChange={(e) => setDesc(e.target.value)}

                                />
                            </Col>
                        </Form.Group>
                        
                        <Form.Group as={Row} controlId="productCaption" className='mt-1'>
                            <Form.Label column sm={2}>
                                Caption
                            </Form.Label>
                            <Col sm={10}>
                                <Form.Control type="text" placeholder="Enter Caption"
                                    onChange={(e) => setCaption(e.target.value)}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="productPrice" className='mt-1'>
                            <Form.Label column sm={2}>
                                Price
                            </Form.Label>
                            <Col sm={10}>
                                <Form.Control type="number" placeholder="Enter Price in Pesos"
                                    onChange={(e) => setPrice(e.target.value)} />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="productWeight" className='mt-1'>
                            <Form.Label column sm={2}>
                                Weight (grams)
                            </Form.Label>
                            <Col sm={10}>
                                <Form.Control type="number" placeholder="Enter Weight in grams"
                                    onChange={(e) => setWeight(e.target.value)}
                                />
                            </Col>
                        </Form.Group>
                        <Form.Group as={Row} controlId="productQuantity" className='mt-1'>
                            <Form.Label column sm={2}>
                                Quantity
                            </Form.Label>
                            <Col sm={10}>
                                <Form.Control type="number" placeholder="Enter quantity"
                                    onChange={(e) => setQty(e.target.value)}
                                />
                            </Col>
                        </Form.Group>
                        <Container fluid className='d-flex m-3 justify-content-end'>

                            <Button variant="warning" className="m-1 p-2" onClick={modalClose}>Cancel</Button>

                            <Button variant="success" className="m-1 p-2" type='submit'>Save</Button>
                        </Container>
                    </Form>

                </Modal.Body>
            </Modal>


        </>
    )

}


